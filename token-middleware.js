const jwt = require('jsonwebtoken');

async function verifyToken(req, res, next) {
  const bearerHeader = req.headers.authorization;

  if(bearerHeader === undefined) {
    return res.sendStatus(403);
  }
  
  const token = bearerHeader.split(' ')[1];

  try {
    await jwt.verify(token, process.env.KEY, {maxAge: 5000})
    next();
  } catch (e) {
    console.log(e);
    return res
      .status(403)
      .send({
        data: {
          error: 'Unauthorized'
        }
      });
  }
}

module.exports = verifyToken;