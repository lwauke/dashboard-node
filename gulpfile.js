const { src, dest, watch } = require('gulp');

var sass = require('gulp-sass');
var minifyCss = require('gulp-csso');
var minify = require('gulp-minify')
 
sass.compiler = require('node-sass');

function css() {
  return src('public/src/stylesheets/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCss())
    .pipe(dest('public/dist/stylesheets'))
}

function js() {
  return src('public/src/javascripts/**/*.js')
  .pipe(minify({
    noSource: true
  }))
  .pipe(dest('public/dist/javascripts'))
}

function watchFiles() {
  watch('public/src/stylesheets/**/*.scss', css);
  watch('public/src/javascripts/**/*.js', js);
}

exports.watch = watchFiles;