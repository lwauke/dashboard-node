import { authRequest, reduceObj } from './helpers-min.js';

(function(){
    const doc = document;
    const inputs = doc.querySelectorAll('input:not([type="submit"])');
    const msg = doc.querySelector('.msg');

    doc.querySelector('input[type="submit"]')
    .addEventListener('click', async e => {        
        e.preventDefault();

        msg.classList.remove('hidden');

        const params = reduceObj(
            inputs,
            (a, {name: n, value: v}) => ({...a, [n]: v})
        );

        const response = await authRequest('/api/users', 'post', params);

        const { data } = await response.json();        

        if(data.error) {            
            msg.innerHTML = data.error;
            return
        }

        msg.textContent = data.success;        
    })    
}());