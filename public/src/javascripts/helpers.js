const authRequest = async (route, method = 'get', body = undefined) => fetch(route, {
    method,
    body: body !== undefined ? JSON.stringify(body) : null,
    headers: {
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
    'Content-Type': 'application/json; charset=UTF-8'
    }
});

const reduceObj = (arr, cb) => [].reduce.call(arr, cb, {});

export { authRequest, reduceObj };