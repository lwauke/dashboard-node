import { authRequest } from './helpers-min.js';

(async function(){
  const response = await authRequest('/api')
  const { data: json } = await response.json();
  const msg = document.querySelector('.msg')
  msg.textContent = json.success ? json.success : json.error;
}())