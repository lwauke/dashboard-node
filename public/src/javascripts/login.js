import { reduceObj } from './helpers-min.js';
(async function(){
  const doc = document
  const inputs = doc.querySelectorAll('input:not([type="submit"])');
  const jsonHeaders = { 'Content-Type': 'application/json; charset=UTF-8' };
  const errMsg = doc.querySelector('.err-msg');

  const request = async body => fetch('/login', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: jsonHeaders
  })

  doc.querySelector('input[type="submit"]')
  .addEventListener('click', async e => {
    e.preventDefault();

    errMsg.classList.add('hidden');
    
    const credentials = reduceObj(
      inputs,
      (a, {name: n, value: v}) => ({...a, [n]: v})
    );
    
    const response = await request(credentials); 

    const { data } = await response.json();

    if (data.error) {
      errMsg.classList.remove('hidden');
      errMsg.textContent = data.error
      return
    }
    
    localStorage.setItem('token', data.success.token)
    location.href = '/'
  })
}());