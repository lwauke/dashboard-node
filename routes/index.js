const express = require('express');
const router = express.Router();

router
.get('/', (req, res, next) => {
  res.render('index', {
    title: 'Dashboard'
  });
})
.get('/users', (req, res, next) => {
  res.render('users', {
    title: 'Usuários'
  });
})
.get('/logout', (req, res, next) => {
  res.render('logout');
})
module.exports = router