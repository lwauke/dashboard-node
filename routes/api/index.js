const express = require('express');
const router = express.Router();
const verifyToken = require('../../token-middleware');
const sequelize = require('../../helpers/sql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router
.get('/', verifyToken, (req, res) => {
  res.json({ data: { success: 'dados' } })
})
.post('/users', verifyToken, async (req, res) => {
  try {
    const { username, password } = req.body;
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    const queryValues = [username, hash].map(v => `'${v}'`).join(',');

    const queryResult = await sequelize.query(`
      INSERT INTO users
      VALUES(DEFAULT, ${queryValues})
    `)

    console.log(queryResult)

    res.json({
      data: {
        success: 'usuário criado'
      }
    })  
  } catch (error) {
    res.json({
      data: {
        error: 'Erro ao inserir usuário'
      }
    });
  }
})
.post('/gatewayiot', async (req, res) => {
  const { token, host, item } = req.body;
  const decoded = jwt.decode(token);
  const grup = decoded.user;

  const zabbixRequest = (method, payload) => axios.post(process.env.ENDPOINT, {
    jsonrpc: 2.0,
    method,
    ...payload
  })

  const { data: { result: auth } } = await zabbixRequest('user.login', {
      params : {
        user: process.env.ZABBIX_USER,
        password: process.env.ZABBIX_PASSWORD
      },
      id : 1
  });

  const { data: { result: grpIotRespponse } } = await zabbixRequest('hostgroup.get', {
      params: {
          output: ['name'],
          search: { name: 'Templates/IOT' }
      },
      auth,
      'id': 1
  });

  const grpIot = grpIotRespponse[0].groupid;

  let { data: { result: grpGet } } = await zabbixRequest('hostgroup.get', {
      params: {
          output: ['name'],
          search: { name: `IOT/${grup}` }
      },
      auth,
      'id': 1
  });

  if(!grpGet.length) {
      const { data: { result: grpMk } } =  await zabbixRequest('hostgroup.create', {
          params: { name: `IOT/${grup}` },
          auth,
          'id': 1
      });

      grpGet = grpMk.groupids[0];
  } else {
      grpGet = grpGet[0].groupid;
  }

  let { data: { result: tmpGet } } = await zabbixRequest('template.get', {
      params: {
          output: ['name'],
          search: { name: `[IOT] ${grup}` }
      },
      auth,
      'id': 1
  });


  if(!tmpGet.length) {
      const { data: { result: tmpMk } } =  await zabbixRequest('template.create', {
          params: {
              host: `IOT ${grup}`,
              name: `[IOT] ${grup}`,
              groups: { groupid: grpIot}
          },
          auth,
          'id': 1
      });

      tmpGet = tmpMk.templateids[0]
  } else {
      tmpGet = tmpGet[0].templateid;
  }

  let { data: { result: hstGet } } = await zabbixRequest('host.get', {
      params: {
          output: ['host', 'name'],
          filter: { host: `${grup}_${host}` }
      },
      auth,
      'id': 1
  });

  if(!hstGet.length) {
      const { data: { result: hstMk } } =  await zabbixRequest('host.create', {
          params: {
              host: `${grup}_${host}`,
              interfaces: [{
                  type: 1,
                  main: 1,
                  useip: 1,
                  ip: "127.0.0.1",
                  dns: "",
                  port: "10050"
              }],
              groups: [{ groupid: grpGet }],
              templates: [{ templateid: tmpGet }]
          },
          auth,
          'id': 1
      });

      hstGet = hstMk.hostids[0];
  } else {
      hstGet = hstGet[0].hostid;
  }

  let { data: { result: itmGet } } = await zabbixRequest('item.get', {
      params: {
          output: ['name', 'key_'],
          templateids: tmpGet, 
    filter: { "key_" : item }
      },
      auth,
      'id': 1
  });

  if(!itmGet.length) {
      const { data: { result: itmMk } } =  await zabbixRequest('item.create', {
          params: {
              name: item,
              'key_': item,
              hostid: tmpGet,
              type: 2,
              'value_type': 0,
          },
          auth,
          'id': 1
      });

      itmGet = itmMk.itemids[0];
  } else {
      itmGet = itmGet[0].itemid;
  }

  await zabbixRequest('user.logout', { params: { auth } });
})

module.exports = router