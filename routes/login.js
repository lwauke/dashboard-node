const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const sequelize = require('../helpers/sql');

const jwt = require('jsonwebtoken');
const promisify = require('util').promisify;

router
.get('/', (req, res, next) => {
  res.render('login');
})
.post('/', async (req, res) => {
  const { user, password } = req.body;

  try {
    const [{password_hash}] = await sequelize.query(
      `SELECT password_hash FROM users WHERE username = '${user}'`,
      { type: sequelize.QueryTypes.SELECT}
    );  
    
    const validUser = await bcrypt.compare(password, password_hash);

    if(!validUser) {
      throw 'invalid user';
    }

    const sign = promisify(jwt.sign);
    const token = await sign({ user }, process.env.KEY, {expiresIn: '1h'});

    res.json({ data: {
      success: { token }
    }});
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      data: { error: 'Usuário ou senha inválidos'},
    });
  }
})


module.exports = router;