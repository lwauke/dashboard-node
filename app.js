const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')

const app = express();

require('dotenv').config()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/static', express.static(path.join(__dirname, 'public', 'dist')));

app.use('/', require('./routes/index'));
app.use('/api', require('./routes/api/index'));
app.use('/login', require('./routes/login'));


const port = 3001;

app.listen(port, () => console.log(`listening on localhost:${port}`));