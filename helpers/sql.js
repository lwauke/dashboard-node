const Sequelize = require('sequelize');

const sequelize = new Sequelize('kaeot', process.env.USER_DB, process.env.PASSWORD_DB, {
    host: 'localhost',
    port: 3306,
    dialect: "mysql",
    pool: {
        max: 20,
        min: 0,
        idleTimeoutMillis: 30000
    },
    logging: false
});

module.exports = sequelize;